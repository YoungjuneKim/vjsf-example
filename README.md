# vsjf-example

## Setup

**Always run scripts from the root directory.**

1. Make sure you are using node >=12. upto 14. `node --version`.
2. Run `npm install`

### I want to...

#### compile and run a hot-reloading dev server for development
```
npm run serve
```

#### compile and minify for production
```
npm run build
```

#### run unit tests
```
npm run test:unit
```

#### run linter and fix files
```
npm run lint
```

#### customize the configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
